package com.captain.utils;

import java.util.UUID;

/**
 * 生成UUID工具
 */
public class UuidUtil {

    public static String getUUID(){
        String uuid=UUID.randomUUID().toString().replace("-","");
        return uuid.toLowerCase();
    }
}
