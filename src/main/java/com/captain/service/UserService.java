package com.captain.service;

import com.captain.dao.UserMapper;
import com.captain.entity.po.User;
import com.captain.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.List;

@Component
public class UserService {
    @Autowired
    UserMapper userMapper;

    public User getUserById(Long id){
        return userMapper.getUserById(id);
    }

    public Integer updateUserSelective(User user){
        return userMapper.updateByPrimaryKeySelective(user);
    }

    public User getUserByEmail(String email){
        return  userMapper.getUserByEmail(email);
    }

    public Integer addUser(User user){
        Date date = new Date();
        user.setCreateTime(date);
        user.setUpdateTime(date);
        return userMapper.insert(user);
    }

}
