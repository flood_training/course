package com.captain.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PostController {

    @GetMapping("/post/add")
    public String addPost(){
        return "post/add";
    }
}
